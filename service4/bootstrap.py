# -*- coding: utf-8 -*-

# 服务5 执行模块
# 1：监听关联模型的决策
# 2：执行决策

from flask import Flask
from flask import request
import requests
import threading
import numpy as np
import pandas as pd

from sklearn.cluster import MiniBatchKMeans
from sklearn.preprocessing import MinMaxScaler
from sklearn import svm
import joblib
import json
from time import sleep, time

jkd_lock = threading.Lock()

app = Flask(__name__)

# TODO 此处的地址应改为service2对应的IP地址
# ipAdd = "http://127.0.0.1:8080"
ipAdd = "http://127.0.0.1:8102"

# 1代表R
# 2代表W
# 3代表RW
# 4代表数据库额所应缓存使用率过高，建议建立新的索引
# 5代表CPU相应负载过高，时间过高，建议清理僵尸进程
# 6代表指标值过大，建议扩充内存
# 7代表值缓存过高，建议增大数据库默认缓存大小
# 8代表CPU线程性能值异常，建议合理配置线程池线程数
# 9代表内部进程的利用率过低，建议关闭不需要的内部进程
# 10代表系统吞吐量过高，建议减少网络开销，适当使用长连接
# 11代表利用率过高，建议关闭不需要的进程
# 12代表数据库处理值数过大，建议增加数据库的连接数，预建立合适数量的TCP连接数
# 13代表IO性能过载，建议创建一个或多个独立的临时表空间
# 14代表平均等待时间过长，建议追加读代替随机读，减少寻址开销，加快 I/O 读的速度
# 一共有17类DBA建议，使用了11种

# 关联模型的决策（DBA建议）
itemID2RW_Map = {
    "23275": "4",  # 数据库额所应缓存使用率过高，建议建立新的索引
    "36636": "1",
    "29174": "5",  # CPU相应负载过高，时间过高，建议清理僵尸进程
    "31272": "6",  # 指标值过大，建议扩充内存
    "23620": "7",  # 值缓存过高，建议增大数据库默认缓存大小
    "29177": "1",
    "23253": "8",  # CPU线程性能值异常，建议合理配置线程池线程数
    "29175": "2",
    "23256": "2",
    "23257": "3",
    "25667": "9",  # 内部进程的利用率过低，建议关闭不需要的内部进程
    "25668": "1",
    "23276": "1",
    "23274": "10",  # 系统吞吐量过高，建议减少网络开销，适当使用长连接
    "29170": "2",
    "29171": "2",
    "29166": "2",
    "29162": "2",
    "23264": "3",
    "29173": "2",
    "29200": "11",  # 利用率过高，建议关闭不需要的进程
    "29823": "1",
    "29172": "3",
    "29176": "1",
    "23273": "1",
    "23277": "12",  # 数据库处理值数过大，建议增加数据库的连接数，预建立合适数量的TCP连接数
    "10076": "13",  # IO性能过载，建议创建一个或多个独立的临时表空间
    "36626": "14",  # 平均等待时间过长，建议追加读代替随机读，减少寻址开销，加快 I/O 读的速度
    "10077": "1",
    "10073": "1"
}

# 错误码对应的DBA决策
RWMap = {
    "1": ["2", "5"],  # 与读操作相关的指标发生了病态故障需要限制所有读操作
    "2": ["3", "4", "6"],  # 与写操作相关的指标发生了病态故障需要限制所有写操作
    "3": ["1", "7"],  # 与读写操作相关的指标发生了病态故障需要限制所有读和写操作
    "4": ["2"],  # 与读操作相关的指标发生了亚健康故障需要限制部分读操作
    "5": ["3"],  # 与写操作相关的指标发生了亚健康故障需要限制部分写操作
    "6": ["7"],  # 与读写操作相关的指标发生了亚健康故障需要限制部分读和写操作
}

# 决策的url 参数
executeMap = {
    "1": "/aiops/service2/api/delete_user?user_count=1",  # 删除用户  (R W)
    "2": "/aiops/service2/api/update/operator_rate?Update=1",  # 更改读写比例 Update+0.1 (R)
    "3": "/aiops/service2/api/update/operator_rate?Select=1",  # 更改读写比例 Select+0.1 (W)
    "4": "/aiops/service2/api/update/time_gap?GapType=Update",  # 更改写间隔 +10s (W)
    "5": "/aiops/service2/api/update/time_gap?GapType=Select",  # 更改读间隔 +10s (R)
    "6": "/aiops/service2/api/update/select_limit",  # 更改查询上限-10 (W)
    "7": "/aiops/service2/api/update/create_user_time",  # 更新创建用户时间+60s (R W)
}


# 设置DBA函数
# 决策提示信息（类似报错信息）
def case23275():  # 4
    # 数据库额所应缓存使用率过高，建议建立新的索引
    print("The cache usage rate of the database is too high. It is recommended to create a new index")


def case29200():  # 11
    # 利用率过高，建议关闭不需要的进程
    print("The utilization rate is too high. It is recommended to close unnecessary processes")


def case31272():  # 6
    # 指标值过大，建议扩充内存
    print("The index value is too large. It is recommended to expand the memory")


def case23253():  # 8
    # CPU线程性能值异常，建议合理配置线程池线程数
    print(
        "The CPU thread performance value is abnormal. It is recommended to reasonably configure the number of "
        "threads in the thread pool")


def case25667():  # 9
    # 内部进程的利用率过低，建议关闭不需要的内部进程
    print(
        "The utilization rate of internal processes is too low. It is recommended to close unnecessary internal "
        "processes")


def case29174():  # 5"
    # CPU相应负载过高，时间过高，建议清理僵尸进程
    print("The CPU load is too high and the time is too high. It is recommended to clean up the zombie process")


def case23274():  # "10"
    # 系统吞吐量过高，建议减少网络开销，适当使用长连接
    print(
        "The system throughput is too high. It is recommended to reduce network overhead and properly use long "
        "connections")  #


def case23620():  # "7"
    # 值缓存过高，建议增大数据库默认缓存大小
    print("The value cache is too high. It is recommended to increase the default database cache size")


def case10076():  # "13"
    # IO性能过载，建议创建一个或多个独立的临时表空间
    print("IO performance is overloaded. It is recommended to create one or more independent temporary tablespaces")


def case23277():  # "12"
    # 数据库处理值数过大，建议增加数据库的连接数，预建立合适数量的TCP连接数
    print(
        "The number of database processing values is too large. It is recommended to increase the number of database "
        "connections and pre establish an appropriate number of TCP connections")


def case36626():  # "14"
    # 平均等待时间过长，建议追加读代替随机读，减少寻址开销，加快 I/O 读的速度
    print(
        "The average waiting time is too long. It is recommended to replace random reading with additional reading to "
        "reduce addressing overhead and speed up I/O reading")


# 汇总上方DBA建议
def DBI_suggestion(id):
    switch = {'4': case23275,  # 注意此处不要加括号
              '11': case29200,
              '6': case31272,
              '8': case23253,
              '9': case25667,
              '5': case29174,
              '10': case23274,
              '7': case23620,
              '13': case10076,
              '12': case23277,
              '14': case36626,
              }
    return switch.get(id)()


'''
Joblib是一个可以简单地将Python代码转换为并行计算模式的软件包，它可非常简单并行我们的程序，从而提高计算速度。
Joblib是一组用于在Python中提供轻量级流水线的工具。 它具有以下功能：
透明的磁盘缓存功能和“懒惰”执行模式，简单的并行计算
Joblib对numpy大型数组进行了特定的优化，简单，快速。
'''
def jkd_predict(temp_x):
    # 调入本地模型
    clf = joblib.load('./model/svc_model.pkl')
    #  clf.predict 对新数据训练后返回预测结果
    return clf.predict(temp_x)


def svm_model_save():
    # 读入数据文件，这里的数据文件要求每一行数据的格式相同
    data_jkd = np.loadtxt('jkdData.dat', delimiter=' ')  # 以空格作为分隔符，读取字符串，格式如：[[1 2 3] [4 5 6]]
    # 取数据前二维，矩阵第二维只返回数组中最后一个数据
    p_new = data_jkd[:, -1:]
    # 构建步长为1，长度为新数组首列元素
    jkd_temp_df = pd.DataFrame(index=np.linspace(1, np.shape(p_new)[0], np.shape(p_new)[0], dtype=int),
                               columns=["jkd1", "jkd2"])
    # 转置，构成单列矩阵
    p_new_T = p_new.reshape(-1, 1)
    # 将jkd_temp_df数组 单独赋值
    jkd_temp_df['jkd1'] = p_new_T
    jkd_temp_df['jkd2'] = p_new_T
    # 获取特征中的值
    jkd_temp_np = jkd_temp_df.values
    # 聚类算法
    # 设置聚类效果调参，单次样本处理数据
    z_pred = MiniBatchKMeans(n_clusters=2, batch_size=3).fit_predict(jkd_temp_np)
    print('model recreat start')   # 模块开始重新创建
    # 支持向量分类器进行调参对目标进行监督学习
    svc = svm.SVC(kernel='linear', C=1.0).fit(jkd_temp_np, z_pred)
    # 保存训练模型
    joblib.dump(svc, './model/svc_model.pkl')
    # 模型重新创建完毕
    print('model recreat success')
    return

# 获取性能指标正常数据的阈值
def get_jkd_yuzi():
    # 调用numpy库中的函数，读取jkdData.dat文件
    data_jkd = np.loadtxt('jkdData.dat', delimiter=' ')
    # 取第一维数组的列表索引为200的数据与第二维数组的最后一个数据
    p_new = data_jkd[-200:, -1:]
    # 生成数据步长为1的临时列表
    jkd_temp_df = pd.DataFrame(index=np.linspace(1, np.shape(p_new)[0], np.shape(p_new)[0], dtype=int),
                               columns=["jkd1", "jkd2"])
    # 对新数组进行转置
    p_new_T = p_new.reshape(-1, 1)
    jkd_temp_df['jkd1'] = p_new_T
    jkd_temp_df['jkd2'] = p_new_T
    # 取值
    jkd_temp_np = jkd_temp_df.values
    # 对新数据训练后返回预测结果
    z_pred = jkd_predict(jkd_temp_np)
    # 定义初始化列表
    lei1_df = pd.DataFrame(columns=["jkd"])
    lei2_df = pd.DataFrame(columns=["jkd"])

    for i in range(p_new_T.shape[0]):
        # 对数据集进行划分
        if z_pred[i] == 0:
            num = lei1_df.shape[0]
            # 若转置后训练数据存在0则存入列表1中并定义相关行列
            lei1_df.at[num + 1, 'jkd'] = p_new[i]
        else:
            num = lei2_df.shape[0]
            lei2_df.at[num + 1, 'jkd'] = p_new[i]
    lei1_np = lei1_df.values
    # 更新阈值，
    if max(lei1_np) == max(p_new):
        print('The threshold of health is set as：%f' % min(lei1_np))
        return min(lei1_np)
    else:
        print('The threshold of health is set as：%f' % max(lei1_np))
        return max(lei1_np)


# 模型更新 每五分钟（300秒）执行一次
def model_refresh():
    while True:
        svm_model_save()
        sleep(300)


# 健康度更新
def jkd_refresh():
    global jkd
    while True:
        sleep(5)
        # python多线程锁类 详细解释：
        # acquire() ：https://blog.csdn.net/cumtb2002/article/details/107796460
        # release()： https: // blog.csdn.net / cumubi7453 / article / details / 107796459
        # acquire() 阻塞调用线程，直到当前使用它的线程将锁解锁
        jkd_lock.acquire()
        jkd = get_jkd_yuzi()
        # release() 释放线程
        jkd_lock.release()
        sleep(75)


# 接收service1发送的请求
@app.route('/aiops/get/error_list/', methods=["POST"])
def get_error_list():
    res = {
        "code": 200,
        "message": "success",
        "data": "参数错误，执行失败"
    }
    # 获取错误信息的 json
    errDataList = request.get_json()  # 获取service1中请求传过来的json数据
    print(errDataList)  # TODO 输出接收到的json数据，可不打印，测试结束后可以注释掉
    """
    分析errDataList 得到strategy 以及次数
    1 统计 R、W、RW影响次数
    2 执行策略
    """
    RW_count = [0, 0, 0, 0, 0, 0, 0]
    stdata = 0
    # 健康度总值
    jkd_total = 0
    # 将错误信息汇总
    for errData in errDataList:
        # global stdata, stdata
        stdata = errData["st"]  # 读取json数据中的st（开始时间）
        jkd_total += errData["temp_jkd"]  # temp
    if stdata > 0:
        # 将错误信息写入jkdData.dat文件
        with open("jkdData.dat", "a+") as fp:
            fp.write("{} {}\n".format(stdata, jkd_total))
    global jkd
    for errData in errDataList:
        itemID = str(errData["itemID"])  # 获取json数据中的itemID（超出阈值的itemID）
        """
        itemID2RW_Map 关联模型的决策（dba建议）
        1代表R
        2代表W
        3代表RW
        """
        if itemID2RW_Map[itemID] in ["1", "2", "3"]:
            # 将线程锁住
            jkd_lock.acquire()
            if jkd_total < jkd:
                # todo 修改统计RW影响次数
                RW_count[int(itemID2RW_Map[itemID]) + 3] += 1
                # 释放线程
                jkd_lock.release()
            else:
                # todo 修改统计RW影响次数
                RW_count[int(itemID2RW_Map[itemID])] += 1
                # 释放线程
                jkd_lock.release()
        # DBA建议增添
        else:
            try:
                # 汇总dba建议
                DBI_suggestion(itemID2RW_Map[itemID])
            except:
                print("WARING:DBA遇到未知错误")

    for i in [1, 2, 3, 4, 5, 6]:
        if RW_count[i] == 0:
            continue
        count = RW_count[i]
        for strategy in RWMap[str(i)]:
            '''if strategy == "1":
                count = count // 10'''
            execute(strategy, count)
            # 123456
    res["data"] = "执行成功"  # 将返回响应体中的data改为执行成功
    return res


def execute(strategy, count):
    """
    发送请求给service2,执行对应操作
    """
    # executeMap 决策的url 参数
    url = ipAdd + executeMap[strategy]
    while count > 0:
        count -= 1
        r = requests.get(url)  # 向service2中指定的决策url发送get请求（这里可以不使用变量r接收请求）


if __name__ == '__main__':
    # model_refresh 模型更新
    t2 = threading.Thread(target=model_refresh)
    # jkd_refresh  健康度更新
    t1 = threading.Thread(target=jkd_refresh)
    t2.start()
    t1.start()
    """
    本来此处如下所示：
    app.run(host="0.0.0.0", port=8080)
    应该将端口改为8104
    """
    app.run(host="0.0.0.0", port=8104)
