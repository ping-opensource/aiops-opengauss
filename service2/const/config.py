# -*- coding: utf-8 -*-

UserNumber = 10  # 模拟用户数量

# OperatorNumber = 10  # 用户操作次数

# 增删改查占比
OperatorRate = {
    "Update": 0.5,  # 更新记录
    "Select": 0.5,  # 查询记录
}

# 默认的增删改查占比 不要修改
DefaultOperatorRate = {
    "Update": 0.5,  # 更新记录
    "Select": 0.5,  # 查询记录
}

# 操作时间隔间范围 单位秒
OperatorGap = {
    "UpdateMaxGap": 60,  # 最长操作间隔
    "UpdateMinGap": 60,  # 最短操作间隔
    "SelectMaxGap": 60,  # 最长操作间隔
    "SelectMinGap": 60,  # 最短操作间隔
}

DBName = "fault_locate"  # 数据库名
