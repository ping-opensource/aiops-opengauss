from service2.init.initUser import initUserData, testUserOpt
from service2.init.initRole import initRoleData, testRoleOpt
from service2.init.initAsset import initAssetData, testAssetOpt

initUserData()
testUserOpt()
initRoleData()
testRoleOpt()
initAssetData()
testAssetOpt()
