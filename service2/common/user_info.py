# -*- coding: utf-8 -*-
from const.config import OperatorGap, DefaultOperatorRate
# from const.config import OperatorNumber
from time import sleep
from init.initUser import UserConfig
from init.initRole import RoleConfig
from init.initAsset import AssetConfig
import random
import threading

class UserInfo():
    def __init__(self, user_id, **kwargs):
        self.userConfig = UserConfig()
        self.roleConfig = RoleConfig()
        self.assetConfig = AssetConfig()
        """
        如果传入的字典有OperatorGap，就将值赋值给该变量，否则就把OperatorGap赋值给该变量，
        OperatorGap在service_get_zabbix_data.const.config下
        """
        self.OperatorGap = kwargs.get("OperatorGap") if kwargs.get("OperatorGap") else OperatorGap
        """
        如果传入的字典有DefaultOperatorRate，就将值赋值给该变量，否则就把DefaultOperatorRate赋值给该变量
        DefaultOperatorRate在service_get_zabbix_data.const.config下
        """
        self.OperatorRate = kwargs.get("DefaultOperatorRate") if kwargs.get("DefaultOperatorRate") else DefaultOperatorRate
        self.id = user_id
        self.selectLimit = 10           # 查询最大数量
        self.open = True                # 默认存活
        self.OperatorOpen = {
            "Update": True,
            "Select": True,
        }
        self.updateCount = 0
        self.selectCount = 0

    def run(self):
        # for i in range(OperatorNumber):
        while True:
            if not self.open:  # 如果用户不存活则结束运行
                return
            nextOpreator = self.next_operator()  # 调用next_operator方法，选择下一个要执行的操作
            if nextOpreator == "Update":
                t = threading.Thread(target=self.update_db_table)  # 开启线程执行update_db_table方法
                t.start()
            elif nextOpreator == "Select":
                t = threading.Thread(target=self.select_db_table)  # 开启线程执行select_db_table方法
                t.start()
            else:
                return
            # 刷新间隔时间
            self.update_gap_time()  # 调用update_gap_time方法
            # 间隔一段时间继续下一次操作
            gapTime = self.get_time_gap(nextOpreator)
            sleep(gapTime)

    def next_operator(self):
        """
        返回值 None说明有错误
        否则返回字符串 代表下一个要进行的操作
        """
        rand_num = random.random()
        sumRate = 0
        for k, v in self.OperatorRate.items():
            sumRate += v
            if rand_num <= sumRate:
                return k
        return None

    def get_time_gap(self, opt):
        """
        在最大时间间隔和最小时间间隔之间，随机获取更新或者查询时间间隔时间
        返回值 None说明有错误
        否则返回字符串 代表下一个要进行的操作
        """
        if opt == "Update":
            return 1.0 * random.randint(self.OperatorGap["UpdateMinGap"] * 1000, self.OperatorGap["UpdateMaxGap"] * 1000) / 1000.0
        elif opt == "Select":
            return 1.0 * random.randint(self.OperatorGap["SelectMinGap"] * 1000, self.OperatorGap["SelectMaxGap"] * 1000) / 1000.0

    def set_operator(self, newOperatorRate):
        """
        判断每一个操作的占比是否正确 以及总和是否为1
        """
        if newOperatorRate["Update"] == 1:
            if self.OperatorRate["Update"] == 1:
                return
            self.OperatorRate["Update"] += 0.1
            self.OperatorRate["Select"] -= 0.1
        else:
            if self.OperatorRate["Select"] == 1:
                return
            self.OperatorRate["Update"] -= 0.1
            self.OperatorRate["Select"] += 0.1

    def set_time_gap(self, GapType):
        if GapType == "Update":
            self.OperatorGap["UpdateMaxGap"] += 10
            self.OperatorGap["UpdateMinGap"] += 10
        elif GapType == "Select":
            self.OperatorGap["SelectMaxGap"] += 10
            self.OperatorGap["SelectMinGap"] += 10

    def opt_which_db_table(self):
        """
        生成一个随机数，以达到随机操作一张表的目的
        """
        return random.randint(1, 3)  # 随机操作一张表

    def update_db_table(self):
        """
        向关联的表中插入数据，单行数据的值包括操作的线程号，时间，以及一个随机数
        """
        self.updateCount += 1  # 更新次数加一
        db = self.opt_which_db_table()  # 调用opt_which_db_table方法，随机生成一个1到3的整数
        # 随机对三张表进行操作
        if db == 1:
            self.userConfig.update_user()  # 更新用户的role_name
        elif db == 2:
            self.roleConfig.update_role()  # 更新角色的asset_maintain
        elif db == 3:
            self.assetConfig.update_asset()  # 更新资产的money

    def select_db_table(self):
        """
        查询关联表中的数据
        """
        self.selectCount += 1  # 查询次数加一
        db = self.opt_which_db_table()
        if db == 1:
            self.userConfig.query_user(self.selectLimit)
        elif db == 2:
            self.roleConfig.query_role(self.selectLimit)
        elif db == 3:
            self.assetConfig.query_asset(self.selectLimit)

# 增加查询数据的效率，查询的速度随查询量的增加而增加
    def update_gap_time(self):
        """
        当查询间隔或者更新间隔等于0时，结束
        """
        if self.selectCount % 10 == 1:  # 每查询10次，最长查询时间间隔和最短查询时间间隔就减少1秒
            if self.OperatorGap["SelectMinGap"] == 0:
                return
            self.OperatorGap["SelectMaxGap"] -= 1
            self.OperatorGap["SelectMinGap"] -= 1
        if self.updateCount % 10 == 1:  # 每更新10次，最长更新时间间隔和最短更新时间间隔就减少1秒
            if self.OperatorGap["UpdateMinGap"] == 0:
                return
            self.OperatorGap["UpdateMaxGap"] -= 1
            self.OperatorGap["UpdateMinGap"] -= 1
        if self.selectCount % 20 == 1:  # 每查询20次，最大查询数量就增加1
            self.selectLimit += 1
