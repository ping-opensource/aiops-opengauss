# -*- coding: utf-8 -*-
import random
import traceback
from sqlalchemy import Column
from sqlalchemy import Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from model.db_util import get_db_session, session_context
from const.config import DBName
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

Base = declarative_base()

# t_asset表的模型类
class AssetConfig(Base):
    __tablename__ = "t_asset"
    asset_no = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    equip_number = Column(String, nullable=False, default=-1)
    equip_name = Column(String, nullable=False, default="")
    money = Column(Float, nullable=False, default=0)



    def __repr__(self):
        """format"""
        return "<asset_no={}, equip_number={}, equip_name={}, money={})>".format(self.asset_no, self.equip_number,
                                                                                 self.equip_name, self.money)

    def insert_record(self, new_scene=None):
        if not new_scene:
            print("记录为空,插入失败")
            return

        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                result = ss.add(new_scene)
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", result)

    def update_asset(self):
        """
        更新资产表的money字段
        """
        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                money = random.randint(0, 10000)  # 随机生成一个0到10000的整数
                # 查询出当前资产，并将当前资产的money字段更改为新生成的money
                user_obj = ss.query(AssetConfig).filter(AssetConfig.equip_number == self.equip_number).update(
                    {"money": money})
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_obj)

    def query_asset(self, SelectAssetLimit):
        """
        查询asset表
        """
        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                rand_equip_number = random.randint(0, 100)  # 随机生成一个0到100的整数
                # 查询出equip_number字段大于等于随机生成的rand_equip_number的数据，查询数据的数量是在user_info中设置好的查询数（10条）
                user_list = ss.query(AssetConfig).filter(AssetConfig.equip_number >= rand_equip_number).limit(
                    SelectAssetLimit).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_list)


def initAssetData():
    """
    初始化t_asset表，在t_asset表中生成100条数据
    （由于开始时，t_asset表中没有任何数据，需要初始化）
    """
    obj = AssetConfig()
    # 生成100个asset
    for index in range(100):
        equip_number = index + 1
        s = AssetConfig(equip_number=str(equip_number), equip_name=str(equip_number), money=0)
        obj.insert_record(s)


def testAssetOpt():
    """
    测试t_asset模型类，查询t_asset表的数据，更新t_asset表的数据
    """
    obj = AssetConfig()  # 创建asset的对象
    obj.query_asset()  # 调用query_asset方法，查询asset表中的数据
    obj.update_asset()  # 调用update_asset方法，更新asset表中的数据
