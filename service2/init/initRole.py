# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import random
import traceback
from sqlalchemy import Column
from sqlalchemy import Integer, String
from sqlalchemy.ext.declarative import declarative_base
from model.db_util import get_db_session, session_context
from const.config import DBName
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

Base = declarative_base()

# t_role表的模型类
class RoleConfig(Base):
    __tablename__ = "t_role"
    role_id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    role_name = Column(String, nullable=False, default=-1)
    # TODO 和表中的字段不一样，表中的字段名为role_desc
    role_desc = Column(String, nullable=False,  default="")
    asset_maintain = Column(Integer, nullable=False, default=1)


    def __repr__(self):
        """format"""
        return "<role_id={}, role_name={}, asset_maintain={}, role_desc={})>".format(self.role_id, self.role_name, self.asset_maintain, self.role_desc)

    def insert_record(self, new_scene=None):
        if not new_scene:
            print("记录为空,插入失败")
            return

        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                result = ss.add(new_scene)
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", result)

    def update_role(self):
        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                new_asset_maintain = random.randint(0, 1)  # 随机生成0或者1
                # 查询出当前角色，并将当前角色的asset_maintain字段更改为新生成的new_asset_maintain
                user_obj = ss.query(RoleConfig).filter(RoleConfig.role_name == self.role_name).update({"asset_maintain": new_asset_maintain})
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_obj)

    def query_role(self, SelectRoleLimit):
        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                rand_asset_maintain = random.randint(0, 1)  # 随机生成0或者1
                # 查询出asset_maintain等于随机生成的rand_asset_maintain的数据，查询数据的数量是在user_info中设置好的查询数（10条）
                user_list = ss.query(RoleConfig).filter(RoleConfig.asset_maintain == rand_asset_maintain).limit(SelectRoleLimit).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_list)

def initRoleData():
    """
    初始化t_role表，在t_role表中生成100条数据
    （由于开始时，t_role表中没有任何数据，需要初始化）
    """
    obj = RoleConfig()
    # 生成100个role
    for index in range(100):
        role_name = index + 1
        asset_maintain = random.randint(0, 1)
        s = RoleConfig(role_name=str(role_name), asset_maintain=asset_maintain)
        obj.insert_record(s)

def testRoleOpt():
    """
    测试t_role模型类，查询t_role表的数据，更新t_role表的数据
    """
    obj = RoleConfig()  # 创建role对象
    obj.query_role()  # 调用query_role方法，查询t_role表中的数据
    obj.update_role()  # 调用update_role方法，更新t_asset表中的数据

#  todo
#  每个用户两个线程 读/写
#  更改更新时间间隔 默认值[30, 60]s
#  更改查询时间间隔 默认值[30, 60]s
#  limit随着时间增大 时间间隔随着时间减小10次操作-1
#  随着时间自动增加用户3分钟+1
#  更改limit 默认值10
#  随机操作表 概率相同
#  减少用户
#  增加用户
