# -*- coding: utf-8 -*-
import random
import traceback
from sqlalchemy import Column
from sqlalchemy import Integer, String
from sqlalchemy.ext.declarative import declarative_base
from model.db_util import get_db_session, session_context
from const.config import DBName
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

Base = declarative_base()


# t_user表的模型类
class UserConfig(Base):
    __tablename__ = "t_user"
    user_id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    user_name = Column(String, nullable=False, default=-1)
    password = Column(String, nullable=False)
    role_name = Column(String, nullable=False, default=0)

    SelectUserLimit = 100
    SelectRoleLimit = 10
    SelectAssetLimit = 10

    def __repr__(self):
        """format"""
        return "<user_id={}, user_name={}, password={}, role_name={})>".format(self.user_id, self.user_name,
                                                                               self.password, self.role_name)

    def insert_record(self, new_scene=None):
        if not new_scene:
            print("记录为空,插入失败")
            return

        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                result = ss.add(new_scene)
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", result)

    def update_user(self):
        _session = get_db_session(db_name=DBName)  # 此处的DBName为AssetManagement，与AssetManagement数据建库建立会话
        with session_context(_session) as ss:
            try:
                new_role_name = random.randint(1, 100)   # 随机生成一个1-100的整数作为新的角色名
                # 查询出当前用户，并将当前用户的role_name字段更改为新生成的new_role_name
                user_obj = ss.query(UserConfig).filter(UserConfig.user_name == self.user_name).update(
                    {"role_name": new_role_name})
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_obj)

    def query_user(self, SelectUserLimit):
        _session = get_db_session(db_name=DBName)
        with session_context(_session) as ss:
            try:
                new_role_name = random.randint(1, 100)  # 随机生成一个1-100的整数作为需要查询的角色名
                # 查询role_name等于随机生成的new_role_name的数据，查询数据的数量是在user_info中设置好的查询数（10条）
                user_list = ss.query(UserConfig).filter(UserConfig.role_name == new_role_name).limit(
                    SelectUserLimit).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", user_list)

    def next_table(self):
        # 下一张操作的表
        pass

    def sleep_time(self):
        # 下一次操作间隔 最大1min 防止死去
        pass

    def set_gap_time(self, type, value):
        # 设置时间间隔区间
        # type r w
        # value + -
        pass

    def run(self):
        # 运行线程 死循环操作
        #  limit随着时间增大 时间间隔随着时间减小10次操作-1
        #  随着时间自动增加用户3分钟+1
        pass


def initUserData():
    """
    初始化t_user表，在t_user表中生成10000条数据
    （由于开始时，t_user表中没有任何数据，需要初始化）
    """
    obj = UserConfig()
    # 生成1W个用户
    for index in range(10000):
        user_name = index + 1
        role_name = random.randint(1, 100)
        s = UserConfig(user_name=str(user_name), password="000000", role_name=str(role_name))
        obj.insert_record(s)


def testUserOpt():
    """
    测试t_user模型类，查询t_user表的数据，更新t_user表的数据
    """
    obj = UserConfig()  # 创建user的对象
    obj.query_user()  # 调用query_user方法，查询t_user表中的数据
    obj.update_user()  # 调用update_user方法，更新t_user表中的数据

#  todo
#  更改更新时间间隔 默认值[30, 60]s
#  更改查询时间间隔 默认值[30, 60]s
#  limit随着时间增大 时间间隔随着时间减小10次操作-1
#  随着时间自动增加用户3分钟+1
#  更改limit 默认值10
#  随机操作表 概率相同
#  减少用户
#  增加用户
#  User内部根据role_name操作role表
#  随机选取id 操作asset表
