# -*- coding: utf-8 -*-
import time

from flask import Flask

import threading
from flask import request
from const.config import UserNumber
from common.user_info import UserInfo

UserList = {}  # 记录所有用户  便于执行后续的命令
UserNum = 0  # 记录生成的用户数量
Mutex = threading.Lock()  # 全局锁
CreateUserGap = 180  # 生成用户时间间隔

app = Flask(__name__)


def user_operator(**param):
    """
    模拟单个用户操作数据库
    """
    global UserList
    thread_id = param["thread_id"]  # 获取thread_id的传参
    user = UserInfo(user_id=thread_id)  # 以获取的thread_id作为参数创建UserInfo对象
    UserList[thread_id] = user  # 将user存入UserList列表中
    user.run()  # 执行user对象的run方法


def run():
    """
    启动线程来创建用户
    """
    try:
        print("UserNumber:{}".format(UserNumber))
        threadList = {}
        global UserNum
        for index in range(UserNumber):
            Mutex.acquire()  # 获取线程锁
            UserNum += 1  # 获取到线程以后，就可以新建用户，所以用户数量加1
            nowNum = UserNum
            Mutex.release()  # 释放线程锁
            threadList[nowNum] = threading.Thread(target=user_operator, kwargs={"thread_id": nowNum})  # 设置线程调用user_operator方法
            threadList[nowNum].start()  # 启动线程
            print("线程{}开始操作".format(nowNum))
    except:
        print("错误：无法开启线程！！！")
    global CreateUserGap
    while True:
        # 每隔n分钟生成一个用户
        time.sleep(CreateUserGap)
        create_user()  # 调用create_user方法
        clear_die_user()  # 调用clear_die_user方法

def clear_die_user():
    """
    清除不存活的用户
    """
    global UserList, UserNum  # 访问全局变量UserList和UserNum
    newUserList = []  # 建立一个新的用户用来存放存活的用户
    for i in range(UserNum):  # 根据用户数量来遍历用户
        if UserList[i].open is True:  # 判断具体用户是否存活
            newUserList.append(UserList[i])  # 若存活，则存入新的用户列表
    UserList = newUserList  # 用新的用户列覆盖旧的用户列表，达到清除不存活的用户
    UserNum = len(UserList)  # 用户数量也更新为存活的用户数


# 接收由service4发送过来的请求
@app.route('/aiops/service2/api/delete_user', methods=["GET"])
def delete_user():
    """
    删除用户 （R W）
    """
    new_val = request.args.get("user_count", type=int, default=-1)
    user_count = new_val
    if new_val == -1:
        return {
            "code": 400,
            "message": "删除用户失败"
        }
    global UserList, UserNum
    for i in range(UserNum):
        if UserList[i].open is True and new_val > 0:
            UserList[i].open = False
            new_val -= 1

    return {
        "code": 200,
        "message": "删除成功{}个用户，失败{}个".format(user_count, new_val)
    }


@app.route('/aiops/service2/api/create_user', methods=["GET"])
def create_user():
    """
    创建用户，同run方法中创建User的方法类似
    """
    global UserNum
    Mutex.acquire()  # 获取线程锁
    UserNum += 1  # 获取到线程以后，就可以新建用户，所以用户数量加1
    nowNum = UserNum
    Mutex.release()  # 释放线程锁
    t = threading.Thread(target=user_operator, kwargs={"thread_id": nowNum})
    t.start()
    print("线程{}开始操作".format(nowNum))
    return {
        "code": 200,
        "message": "创建用户{}成功".format(nowNum)
    }


@app.route('/aiops/service2/api/update/operator_rate', methods=["GET"])
def update_operator():
    # 增删改查占比
    newOperator = {
        "Update": request.args.get("Update", type=float, default=-1),  # 增加记录
        "Select": request.args.get("Select", type=float, default=-1),  # 查询记录
    }
    global UserList, UserNum
    for i in range(UserNum):
        UserList[i].set_operator(newOperator)
    return {
        "code": 200,
        "message": "更新成功"
    }


@app.route('/aiops/service2/api/update/time_gap', methods=["GET"])
def update_time_gap():
    GapType = request.args.get("GapType", type=str, default=""),  # 增加记录，最长操作间隔
    global UserList, UserNum
    for i in range(UserNum):  # 遍历用户
        UserList[i].set_time_gap(GapType)
    return {
        "code": 200,
        "message": "设置成功"
    }

@app.route('/aiops/service2/api/update/select_limit', methods=["GET"])
def update_select_limit():
    global UserList, UserNum
    for i in range(UserNum):
        UserList[i].selectLimit -= 10
    return {
        "code": 200,
        "message": "设置成功"
    }

def set_create_user_gap():
    global CreateUserGap
    CreateUserGap += 60


@app.route('/aiops/service2/api/update/create_user_time', methods=["GET"])
def update_create_user():
    set_create_user_gap()
    return {
        "code": 200,
        "message": "ok"
    }


if __name__ == '__main__':
    t = threading.Thread(target=run)  # 开启线程执行run方法
    t.start()
    app.run(host="0.0.0.0", port=8102)
