# -*- coding: utf-8 -*-

import traceback
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from sqlalchemy.orm import sessionmaker, scoped_session

DB_CONN = None

DB_ENGINE = None
DB_SESSION = None

DB_RR_ENGINE = None
DB_RR_SESSION = None


# 创建连接数据库引擎
def pick_engine(db_name="", mode_rr=False):
    DSN = "opengauss+psycopg2://dhf001:dhf_1020@106.54.40.23:26000/{db_name}".format(
        db_name=db_name)

    """
    使用了sqlalchemy中的 create_engine 函数
    DSN 连接信息
    poolclass=NullPool 禁用连接池
    encoding='utf-8' 仅用于python2中，缺省值为 utf-8，可省略不写
    echo=True 引擎将记录所有语句以及 repr() 其参数列表的默认日志处理程序
    """
    DB_ENGINE = create_engine(DSN, poolclass=NullPool, encoding='utf-8', echo=True)
    if mode_rr:
        """
        execution_options 设置事务隔离级别为可重复读
        """
        DB_RR_ENGINE = DB_ENGINE.execution_options(isolation_level="REPEATABLE READ")
        return DB_RR_ENGINE
    return DB_ENGINE


"""建立与数据库连接的session，未指定隔离级别"""


def get_db_session(db_name="", mode_rr=False):
    """设置为全局变量，以保证为单线程"""
    global DB_SESSION
    if DB_SESSION:
        return DB_SESSION
    """
    sessionmaker()创建了一个工厂类，每次实例化Session都会创建一个绑定了引擎的Session
    session_factory在访问数据库时都会通过这个绑定好的引擎来获取连接资源
    """
    session_factory = sessionmaker(bind=pick_engine(db_name))
    # scoped_session 线程安全
    """
    session不是线程安全的，在多线程的环境中，默认情况下，多个线程将会共享同一个session
    scoped_session类似单例模式，当我们调用使用的时候，会先在Registry里找找之前是否已经创建Session，
    未创建则创建 Session ，已创建则直接返回，只在单线程下发挥作用
    具体来说：
        一个对象一旦被一个 Session 添加，除非关闭这个 Session ，不然其他的 Session 无法添加这个对象。
        一个 Session 添加并提交一个对象，然后关闭该 Session ，其他的 Session 可以添加并提交这个对象，
        但是数据库并不会有这条数据。
    使用scoped_session可以避免这一情况
    """
    DB_SESSION = scoped_session(session_factory)
    return DB_SESSION


"""建立与数据库连接的session，隔离级别为可重复读"""


def get_db_rr_mode_session(psm="", db_name="", mode_rr=False):
    """mode=repeatable read"""
    global DB_RR_SESSION
    if DB_RR_SESSION:
        return DB_RR_SESSION
    """调用pick_engine函数，使隔离级别为可重复读"""
    session_factory = sessionmaker(bind=pick_engine(db_name, True))
    DB_RR_SESSION = scoped_session(session_factory)
    return DB_RR_SESSION


@contextmanager
def session_context(session):
    try:
        """程序会生成大量session，逐个提交消耗时间长，使用yield可以避免"""
        yield session
        session.commit()
    except:
        session.rollback()
        raise Exception(traceback.format_exc())
    finally:
        session.close()
