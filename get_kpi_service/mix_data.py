# -*- coding: utf-8 -*-
from const.config import OperatorRate


class DataStruct:
    def __init__(self, opt):
        self.item = {}
        self.opt = opt


class MixData:
    def __init__(self, optFile, zabbixDataFileList, saveDataFile, itemIdList):
        self.optFile = open(optFile, "r")  # 赋予传入的文件读的权限
        self.inDataFileList = zabbixDataFileList
        self.outDataFile = open(saveDataFile, "w")  # 赋予文件写权限
        self.dataMap = {}  # 时间戳作为key过滤无效数据
        # dataMap中存储的数据：
        # {时间戳1 : [item : {itemId : -1}, opt : 操作序号], 时间戳2 : [item : {itemId : -1}, opt : 操作序号]}
        self.itemIdList = itemIdList

    def loadKeyAndOpt(self):
        """
        加载操作文件中的“时间戳 ”和”对应操作 “
        :return:
        """
        if not self.optFile:  # 如果没有传入操作对应的文件则结束
            return
        print("loadKeyAndOpt\n")
        while True:
            line = self.optFile.readline()  # 读取操作文件中的一行数据
            if not line:  # 如果没有读到数据就结束
                return
            dataList = line.split()  # 以空格为分隔符将从文件中读到的一行进行分割
            self.dataMap[dataList[0]] = DataStruct(dataList[1])  # 将时间戳和操作相对应，存储到dataMap中（操作的类型是DataStruct）
            for itemID in self.itemIdList:  # 遍历设定的ItemId
                self.dataMap[dataList[0]].item[itemID] = -1  # 将对应时间戳的操作的item属性中的key赋值为对应ItemId，value赋值为-1

    def loadZabbixItemData(self, index):
        """
        加载zabbix文件中ItemId对应的值
        :param index: zabbix文件列表中的下标
        :return:
        """
        print("load file {}\n".format(self.inDataFileList[index]))
        fp = open(self.inDataFileList[index], "r")  # 赋予对应zabbix文件读的权限
        while True:  # 读取zabbix文件中的所有数据
            line = fp.readline()
            if not line:
                break
            dataList = line.split()  # 以空格为分隔符将从文件中读到的一行进行分割
            self.dataMap[timeStamp].item[dataList[0]] = dataList[2]  # 如果zabbix的时间戳在操作文件中，则将对应的value值由-1变为zabbix第三列对应的数据
            if timeStamp in self.dataMap.keys():  # 判断zabbix的时间戳是否在操作文件中
                pass
            timeStamp = dataList[1]  # 获取第一行的第二列数据
        fp.close()

    def writeFileHead(self):
        """
        在混合矩阵文件中写入矩阵的行
        :return:
        """
        self.outDataFile.write("timestamp\topt\t")
        for itemId in self.itemIdList:
            self.outDataFile.write("{}\t".format(itemId))
        self.outDataFile.write("\n")

    def sortDataAndSave(self):
        """
        将 MixData类中加载好的时间戳，itemId等数据进行排序，并写入矩阵文件
        :return:
        """
        print("start sort\n")
        for k in sorted(self.dataMap):  # 按时间戳排序，并遍历时间戳
            self.outDataFile.write("{}\t{}\t".format(k, self.dataMap[k].opt))  # 向要写入的文件中写入“时间戳”和“时间戳对应的操作序号”
            for itemId in self.itemIdList:
                self.outDataFile.write("{}\t".format(self.dataMap[k].item[itemId]))  # 将时间戳对应的itemId写入文件
            self.outDataFile.write("\n")
        print("end sort\n")

    def loadFile(self):
        """
        控制 加载操作文件和zabbix文件中的数据，并排序，写入新的文件 的流程
        :return:
        """
        self.loadKeyAndOpt()  # 调用loadKeyAndOpt方法，加载时间戳和对应操作
        for index in range(len(self.inDataFileList)):  # 遍历所有的zabbix数据文件
            self.loadZabbixItemData(index)  # 调用loadZabbixItemData方法，遍历加载对应的zabbix数据文件
        print("load file end\n")
        self.writeFileHead()  # 调用writeFileHead方法，向文件中写入矩阵行
        self.sortDataAndSave()  # 调用sortDataAndSave方法，将数据排序并写入文件


if __name__ == "__main__":
    itemIdList = ["10073", "10074", "10075", "10076", "10077",
                  "10078", "23252", "23253", "23255", "23256",
                  "23257", "23258", "23259", "23260", "23261",
                  "23268", "23269", "23270", "23271", "23272",
                  "23273", "23274", "23275", "23276", "23277",
                  "23287", "23288", "23327", "23328", "23620",
                  "23625", "23628", "23635", "23662", "23664",
                  "25367", "25371", "25667", "25668", "28249",
                  "28534", "28536", "28538", "29161", "29162",
                  "23262", "23264", "23265", "23266", "23267",
                  "29163", "29164", "29165", "29166", "29167",
                  "29168", "29169", "29170", "29171", "29172",
                  "29173", "29174", "29175", "29176", "29177",
                  "29178", "29179", "29180", "29181", "29182",
                  "29184", "29186", "29187", "29188", "29189",
                  "29190", "29191", "29192", "29193", "29194",
                  "29195", "29185", "29196", "29197", "29200",
                  "29557", "29823", "31272", "34196", "36625",
                  "36626", "36627", "36628", "36629", "36630",
                  "36631", "36632", "36633", "36634", "36635",
                  "36636", "36637", "36638", "36639", "36640",
                  "36641", "36642", "36643", "36644", "36645"]

    zabbixDataFileList = []
    optDataFile = "data_{}.txt".format(int(OperatorRate["Insert"] * 100))
    # TODO 此处的文件输出路径根据文档要求，需要改，改到data目录下，而不是当前目录下
    outputDataFile = ".\data\mix_data_{}.txt".format(int(OperatorRate["Insert"] * 100))
    for item in itemIdList:
        fileName = r".\zabbix_data\{}_DB_{}.txt".format(item, int(OperatorRate["Insert"] * 100))
        zabbixDataFileList.append(fileName)
    obj = MixData(optDataFile, zabbixDataFileList, outputDataFile, itemIdList)
    obj.loadFile()
    pass
