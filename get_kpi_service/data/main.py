import os, time, itertools, imageio, pickle
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import pandas as pd
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import MinMaxScaler

# txt格式读
mix_data_0 = pd.read_csv('.\mix_data_0.txt', sep='\t')
mix_data_50 = pd.read_csv('.\mix_data_50.txt', sep='\t')
mix_data_100 = pd.read_csv('.\mix_data_100.txt', sep='\t')
# mix_data_simple= pd.read_csv('.\mix_data_simple.txt', sep='\t')

# 取110列
mix_0 = mix_data_0.iloc[0:10001, 2:-1]
data_0_label = mix_data_0.iloc[0:10001, 1]
mix_50 = mix_data_50.iloc[0:10001, 2:-1]
data_50_label = mix_data_50.iloc[0:10001, 1]
mix_100 = mix_data_100.iloc[0:10001, 2:-1]
data_100_label = mix_data_100.iloc[0:10001, 1]
# '''mix_simple=mix_data_simple.iloc[0:10001, 2:52]
# data_simple_label = mix_data_simple.iloc[0:10001, 1]'''

# 样本正则化
'''
#normalizer = Normalizer(norm='l2')  # L2范式
data_0 = normalizer.transform(mix_0)
data_50 = normalizer.transform(mix_50)
data_100 = normalizer.transform(mix_100)'''
# data_simple = normalizer.transform(mix_simple)'''


'''30000条数据
def list_groups(list1,list2,list3):
    zu = -1
    result = []
    for i in list1:
        zu += 1
        result.append([])
        result[zu].extend(i)
    for i in list2:
        zu += 1
        result.append([])
        result[zu].extend(i)
    for i in list3:
        zu += 1
        result.append([])
        result[zu].extend(i)
    return result'''

'''def list_groups(list1):
    zu = -1
    result = []
    for i in list1:
        zu += 1
        result.append([])
        result[zu].extend((i-0.5)/0.5)
    return result'''


def list_groups(list1):
    list2 = (list1 - 0.5) / 0.5

    return list2


def get_mean(list1):
    temp = []
    jieguo = []
    for i in range(list1.shape[1]):
        temp.append([])
        jieguo.append([])
        count = 0
    for i in list1:
        for x in list1[i]:
            if (x > 0.0):
                temp[count].append(x)
        if not temp[count]:
            temp[count].append(0)
        count += 1
    for q in range(list1.shape[1]):
        jieguo[q] = np.mean(temp[q])
    return jieguo


train_old_mean = get_mean(mix_50)


# 重建训练数据
def reshape_train_data(dataframe1, list2):
    start_time = time.time()
    df = dataframe1
    print('开始重构训练数据')
    count = 0
    for i in df:
        df[i] = df[i].map(lambda x: x if x > 0.0 else list2[count])
        count += 1
    end_time = time.time()
    total_time = end_time - start_time
    print('训练集重构完成，耗时:%.2f秒' % (total_time))
    return df


def gan_generate_data_reshape(dataframe1):
    start_time = time.time()
    df = dataframe1
    print('开始重构训练数据')
    count = 0
    for i in df:
        df[i] = df[i].map(lambda x: x if x > 0.0005 else 0)
        count += 1
    end_time = time.time()
    total_time = end_time - start_time
    print('训练集重构完成，耗时:%.2f秒' % (total_time))
    return df


'''旧重构代码
def reshape_train_data(dataframe1,list2):
    start_time = time.time()
    print('开始重构训练数据')
    df=dataframe1
    for x in range (dataframe1.shape[0]):
        for y in range (dataframe1.shape[1]):
            if (dataframe1.iat[x,y] > 0.0):
                df.iat[x,y]=dataframe1.iat[x,y]
            else:
                df.iat[x,y]=list2[y]
    end_time = time.time()
    total_time = end_time - start_time
    print('训练集重构完成，耗时:%.2f秒' % (total_time))
    return df'''

# train_data = list_groups(data_0,data_100,data_50)
train_new_data = reshape_train_data(mix_50, train_old_mean)
mm = MinMaxScaler()
data_50 = mm.fit_transform(train_new_data)
train_data = list_groups(data_50)

'''def label_grouos(list1,list2,list3):
    zu = -1
    result = []
    for i in list1:
        zu += 1
        result.append([])
        result[zu].append(i-1)
    for i in list2:
        zu += 1
        result.append([])
        result[zu].append(i-1)
    for i in list3:
        zu += 1
        result.append([])
        result[zu].append(i-1)
    return result
label_data = label_grouos(data_0_label,data_100_label,data_50_label)'''


def label_grouos(list1):
    zu = -1
    result = []
    for i in list1:
        zu += 1
        result.append([])
        result[zu].append(i - 1)
    return result


label_data = label_grouos(data_50_label)
lab = np.array(label_data)
train_labels = np.eye(2)[lab.reshape(-1)]

'''#设置训练集样本 按批次选5个连续操作
def list_of_groups(list_info,list_len):
    zu = 0
    temp = 0
    result = []
    for i in list_info:
        temp += 1
        if(temp == 1):
            result.append([])
        result[zu].extend(i)
        if (temp >= list_len):
            temp = 0
            zu += 1
    return result
train_data_0 = list_of_groups(data_0, 5)
train_data_50 = list_of_groups(data_50, 5)
train_data_100 = list_of_groups(data_100, 5)'''


# leaky_relu
def lrelu(X, leak=0.2):
    f1 = 0.5 * (1 + leak)
    f2 = 0.5 * (1 - leak)
    return f1 * X + f2 * tf.abs(X)


# G(z)
def generator(x, y, isTrain=True, reuse=False):
    with tf.variable_scope('generator', reuse=reuse):
        Wij1 = tf.contrib.layers.variance_scaling_initializer()
        Wij2 = tf.contrib.layers.xavier_initializer()
        G_inputs = tf.concat([x, y], 1)

        dense1 = tf.layers.dense(G_inputs, 55, kernel_initializer=Wij1)
        relu1 = tf.nn.relu(dense1)

        dense2 = tf.layers.dense(relu1, 110, kernel_initializer=Wij2)
        Lg = tf.nn.tanh(dense2)

        return Lg


# D(x)
def discriminator(x, y, isTrain=True, reuse=False):
    with tf.variable_scope('discriminator', reuse=reuse):
        Wij1 = tf.contrib.layers.variance_scaling_initializer()
        Wij2 = tf.contrib.layers.xavier_initializer()

        D_inputs = tf.concat([x, y], 1)

        dense1 = tf.layers.dense(D_inputs, 55, kernel_initializer=Wij1)
        lrelu1 = lrelu(dense1, 0.2)  # leak值设定

        dense2 = tf.layers.dense(lrelu1, 110, kernel_initializer=Wij1)
        lrelu2 = lrelu(dense2, 0.1)

        dense3 = tf.layers.dense(lrelu2, 1, kernel_initializer=Wij2)
        Ld = tf.nn.sigmoid(dense3)

        return Ld, dense3


# label preprocess
onehot = np.eye(2)

temp_z_ = np.random.normal(0, 1, (2, 110))  # 原50
fixed_z_ = temp_z_
fixed_y_ = np.zeros((25, 1))

for i in range(1):
    fixed_z_ = np.concatenate([fixed_z_, temp_z_], 0)  # 100*100
    temp = np.ones((25, 1)) + i
    fixed_y_ = np.concatenate([fixed_y_, temp], 0)

fixed_y_ = onehot[fixed_y_.astype(np.int32)].squeeze()  # 100个标签


def data_output(array1):  # raise ValueError("cannot set a row with mismatched columns")
    # ValueError: cannot set a row with mismatched columns
    out_put_df = pd.DataFrame(index=np.linspace(1, 10000, 10000, dtype=int), columns=list(train_new_data))
    count_line = 0
    for i in array1:
        for line in i:
            predict_value = mm.inverse_transform(line * 0.5 + 0.5)
            # with open('newdata.txt','a+'):
            np.savetxt('newdata.txt', predict_value)
            out_put_df.loc[count_line] = predict_value
            count_line += 1
    return out_put_df


'''def show_result(num_epoch, show = False, save = False, path = 'result.png'):
    test_images = sess.run(G_z, {z: fixed_z_, y: fixed_y_, isTrain: False})

    size_figure_grid = 10
    fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(10, 10))
    for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
        ax[i, j].get_xaxis().set_visible(False)
        ax[i, j].get_yaxis().set_visible(False)

    for k in range(size_figure_grid*size_figure_grid):
        i = k // size_figure_grid
        j = k % size_figure_grid
        ax[i, j].cla()
        ax[i, j].imshow(np.reshape(test_images[k], (28, 28)), cmap='gray')

    label = 'Epoch {0}'.format(num_epoch)
    fig.text(0.5, 0.04, label, ha='center')

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()'''


def show_train_hist(hist, show=False, save=False, path='Train_hist.png'):
    x = range(len(hist['D_losses']))

    y1 = hist['D_losses']
    y2 = hist['G_losses']

    plt.plot(x, y1, label='D_loss')
    plt.plot(x, y2, label='G_loss')

    plt.xlabel('Epoch')
    plt.ylabel('Loss')

    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()


# training parameters
batch_size = 100
lr = 0.0002  # 学习率
train_epoch = 100

train_set = train_data  # normalization; range: -1 ~ 1
train_label = train_labels

# variables : input  占位
x = tf.placeholder(tf.float32, shape=(None, 110))  # 原50
y = tf.placeholder(tf.float32, shape=(None, 2))
z = tf.placeholder(tf.float32, shape=(None, 110))  # 原50
isTrain = tf.placeholder(dtype=tf.bool)

# networks : generator
G_z = generator(z, y, isTrain)

# networks : discriminator
D_real, D_real_logits = discriminator(x, y, isTrain)
D_fake, D_fake_logits = discriminator(G_z, y, isTrain, reuse=True)

# loss for each network

D_loss_real = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=D_real_logits, labels=tf.ones([batch_size, 1])))  # 均值
D_loss_fake = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=D_fake_logits, labels=tf.zeros([batch_size, 1])))
D_loss = D_loss_real + D_loss_fake
G_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=D_fake_logits, labels=tf.ones([batch_size, 1])))

# trainable variables for each network
T_vars = tf.trainable_variables()  # 网络训练参数列表
D_vars = [var for var in T_vars if var.name.startswith('discriminator')]
G_vars = [var for var in T_vars if var.name.startswith('generator')]

# optimizer for each network
with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):  # 定义非梯度更新参数训练顺序
    D_optim = tf.train.AdamOptimizer(lr, beta1=0.8).minimize(D_loss, var_list=D_vars)  # 寻找最优点  衰减率设置
    G_optim = tf.train.AdamOptimizer(lr, beta1=0.8).minimize(G_loss, var_list=G_vars)

# open session and initialize all variables
sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

# results save folder
root = 'cGAN_results/'
model = 'cGAN_'
if not os.path.isdir(root):
    os.mkdir(root)
if not os.path.isdir(root + 'Fixed_results'):
    os.mkdir(root + 'Fixed_results')

train_hist = {}
train_hist['D_losses'] = []
train_hist['G_losses'] = []
train_hist['per_epoch_ptimes'] = []
train_hist['total_ptime'] = []

if __name__ == "__main__":
    # training-loop
    np.random.seed(int(time.time()))
    print('训练开始!')
    start_time = time.time()
    for epoch in range(train_epoch):
        G_losses = []
        D_losses = []
        epoch_start_time = time.time()
        for iter in range(len(train_set) // batch_size):
            # update discriminator
            x_ = train_set[iter * batch_size:(iter + 1) * batch_size]
            y_ = train_label[iter * batch_size:(iter + 1) * batch_size]

            z_ = np.random.normal(0, 1, (batch_size, 110))  # 原50

            loss_d_, _ = sess.run([D_loss, D_optim], {x: x_, y: y_, z: z_, isTrain: True})
            D_losses.append(loss_d_)  # 生成器曲线

            # update generator
            z_ = np.random.normal(0, 1, (batch_size, 110))  # 原50
            y_ = np.random.randint(0, 1, (batch_size, 1))
            y_ = onehot[y_.astype(np.int32)].squeeze()
            loss_g_, _ = sess.run([G_loss, G_optim], {z: z_, x: x_, y: y_, isTrain: True})
            G_losses.append(loss_g_)  # 辨别器曲线

        epoch_end_time = time.time()
        per_epoch_ptime = epoch_end_time - epoch_start_time
        print('[%d/%d] - 训练时间: %.2f loss_d: %.3f, loss_g: %.3f' % ((epoch + 1), train_epoch, per_epoch_ptime,
                                                                       np.mean(D_losses), np.mean(G_losses)))
        # fixed_p = root + 'Fixed_results/' + model + str(epoch + 1) + '.png'
        # show_result((epoch + 1), save=True, path=fixed_p)
        train_hist['D_losses'].append(np.mean(D_losses))
        train_hist['G_losses'].append(np.mean(G_losses))
        train_hist['per_epoch_ptimes'].append(per_epoch_ptime)

    end_time = time.time()
    total_ptime = end_time - start_time
    train_hist['total_ptime'].append(total_ptime)

    print('平均单次训练时间: %.2f, 全部 %d 次训练耗时: %.2f' % (
    np.mean(train_hist['per_epoch_ptimes']), train_epoch, total_ptime))
    print("训练完成!... 训练数据保存成功")
    with open(root + model + 'train_hist.pkl', 'wb') as f:
        pickle.dump(train_hist, f)

    show_train_hist(train_hist, save=True, path=root + model + 'train_hist.png')

    # 生成数据库数据
    # 增强数据标签设置
    print('开始生成增强数据')
    data_ds_all = []
    label_all = []
    for item_ds in range(100):
        z_ = np.random.normal(0, 1, (100, 110))  # 原110
        y_ = np.random.randint(0, 1, (100, 1))
        label_all.append(y_)
        y_ = onehot[y_.astype(np.int32)].squeeze()
        data_ds = sess.run([G_z], {z: z_, x: x_, y: y_, isTrain: False})
        data_ds_all.append(data_ds)

    # = generator(x_ds,y_ds,isTrain=False)
    # new_data = data_output(data_ds_all)
    for i in label_all:
        for line in i:
            temp_line = line.reshape(-1, 1)
            label_line = temp_line.astype('int32')
            with open('labeldata.txt', 'ab') as f:
                np.savetxt(f, label_line, fmt='%d', delimiter='\t')
    # out_put_df = pd.DataFrame(index=np.linspace(1, 10000, 10000, dtype=int), columns=list(train_new_data))
    #    count_line = 0
    for i in data_ds_all:
        for line in i:
            predict_value = mm.inverse_transform(line * 0.5 + 0.5)
            temp_line = predict_value.reshape(-1, 110)  # 原50
            new_line = temp_line.astype('float64')
            with open('newdata.txt', 'ab') as f:
                np.savetxt(f, new_line, fmt='%12f', delimiter='\t')

    print('增强数据完成生成')

    sess.close()

    gan_generate_data = np.loadtxt('./newdata.txt')
    gan_df = pd.DataFrame(gan_generate_data, index=np.linspace(0, 9999, 10000, dtype=int),
                          columns=mix_50.columns.values)
    new_gan_df = gan_generate_data_reshape(gan_df)
    new_gan_df.to_csv('new_gan_data.txt', sep='\t', index=True, header=True)
