﻿import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.stats import norm

# 读取数据，csv格式读
# mix_data_0=pd.read_csv(r'data\mix_data_0.csv',encoding='utf8')
# mix_data_50=pd.read_csv(r'data\mix_data_50.csv',encoding='utf8')
# mix_data_100=pd.read_csv(r'data\mix_data_100.csv',encoding='utf8')
# mix_data_simple=pd.read_csv(r'data\mix_data_simple.csv',encoding='utf8')

# txt格式读
# 这里读取文件的路径是当前目录下的mix_data_{}.txt
mix_data_0 = pd.read_csv('mix_data_0.txt', sep='\t')
mix_data_50 = pd.read_csv('mix_data_50.txt', sep='\t')
mix_data_100 = pd.read_csv('mix_data_100.txt', sep='\t')
# mix_data_simple= pd.read_csv('mix_data_simple.txt', sep='\t')

# 取50列
mix_data_0 = mix_data_0.iloc[:, 2:-1]
mix_data_50 = mix_data_50.iloc[:, 2:-1]
mix_data_100 = mix_data_100.iloc[:, 2:-1]
# mix_data_simple=mix_data_simple.iloc[:, 2:-1]


# 将数据中无用的0和-1数据剔除
mix_data_0 = mix_data_0.replace([-1, 0], np.nan)
mix_data_50 = mix_data_50.replace([-1, 0], np.nan)
mix_data_100 = mix_data_100.replace([-1, 0], np.nan)
# mix_data_simple = mix_data_simple.replace([-1,0],np.nan)


mix_data_0_count = mix_data_0.isnull().sum()
mix_data_0_count = mix_data_0_count[mix_data_0_count < 6500].index.tolist()
mix_data_0 = mix_data_0[mix_data_0_count]

mix_data_50_count = mix_data_50.isnull().sum()
mix_data_50_count = mix_data_50_count[mix_data_50_count < 6500].index.tolist()
mix_data_50 = mix_data_50[mix_data_50_count]

mix_data_100_count = mix_data_100.isnull().sum()
mix_data_100_count = mix_data_100_count[mix_data_100_count < 6500].index.tolist()
mix_data_100 = mix_data_100[mix_data_100_count]

# mix_data_simple_count = mix_data_simple.isnull().sum()
# mix_data_simple_count = mix_data_simple_count[mix_data_simple_count<6500].index.tolist()
# mix_data_simple = mix_data_simple[mix_data_simple_count]

# 归一化
mix_data_0 = (mix_data_0 - mix_data_0.mean(axis=0)) / (mix_data_0.max(axis=0) - mix_data_0.min(axis=0))
mix_data_50 = (mix_data_50 - mix_data_50.mean(axis=0)) / (mix_data_50.max(axis=0) - mix_data_50.min(axis=0))
mix_data_100 = (mix_data_100 - mix_data_100.mean(axis=0)) / (mix_data_100.max(axis=0) - mix_data_100.min(axis=0))
# mix_data_simple = (mix_data_simple - mix_data_simple.mean(axis=0))/ (mix_data_simple.max(axis=0)-mix_data_simple.min(axis=0))


# 求方差
mix_data_0_var = mix_data_0.var()
mix_data_50_var = mix_data_50.var()
mix_data_100_var = mix_data_100.var()
# mix_data_simple_var=mix_data_simple.var()

# 方差排序
mix_data_0_var_sort = mix_data_0_var.sort_values(ascending=False)  # 从大到小排序
mix_data_50_var_sort = mix_data_50_var.sort_values(ascending=False)
mix_data_100_var_sort = mix_data_100_var.sort_values(ascending=False)
# mix_data_simple_var_sort=mix_data_simple_var.sort_values(ascending = False)

# 保存方差排序的结果
mix_data_0_var_sort.to_excel('data1\\var\\mix_data_0_var_sort.xlsx')
mix_data_50_var_sort.to_excel('data1\\var\\mix_data_50_var_sort.xlsx')
mix_data_100_var_sort.to_excel('data1\\var\\mix_data_100_var_sort.xlsx')
# mix_data_simple_var_sort.to_excel('data\\var\\mix_data_simple_var_sort.xlsx')


# 求均值
mix_data_0_mean = mix_data_0.mean()
mix_data_50_mean = mix_data_50.mean()
mix_data_100_mean = mix_data_100.mean()
# mix_data_simple_mean=mix_data_simple.mean()

# 均值排序
mix_data_0_mean_sort = mix_data_0_mean.sort_values(ascending=False)  # 从大到小排序
mix_data_50_mean_sort = mix_data_50_mean.sort_values(ascending=False)
mix_data_100_mean_sort = mix_data_100_mean.sort_values(ascending=False)
# mix_data_simple_mean_sort=mix_data_simple_mean.sort_values(ascending = False)

# 保存均值排序的结果
mix_data_0_mean_sort.to_excel('data1\\mean\\mix_data_0_mean_sort.xlsx')
mix_data_50_mean_sort.to_excel('data1\\mean\\mix_data_50_mean_sort.xlsx')
mix_data_100_mean_sort.to_excel('data1\\mean\\mix_data_100_mean_sort.xlsx')
# mix_data_simple_mean_sort.to_excel('data\\mean\\mix_data_simple_mean_sort.xlsx')

# 求中位数
mix_data_0_median = mix_data_0.median()
mix_data_50_median = mix_data_50.median()
mix_data_100_median = mix_data_100.median()
# mix_data_simple_median=mix_data_simple.median()

# 中位数排序
mix_data_0_median_sort = mix_data_0_median.sort_values(ascending=False)  # 从大到小排序
mix_data_50_median_sort = mix_data_50_median.sort_values(ascending=False)
mix_data_100_median_sort = mix_data_100_median.sort_values(ascending=False)
# mix_data_simple_median_sort=mix_data_simple_median.sort_values(ascending = False)

# 保存中位数排序的结果
mix_data_0_median_sort.to_excel('data1\\median\\mix_data_0_median_sort.xlsx')
mix_data_50_median_sort.to_excel('data1\\median\\mix_data_50_median_sort.xlsx')
mix_data_100_median_sort.to_excel('data1\\median\\mix_data_100_median_sort.xlsx')
# mix_data_simple_median_sort.to_excel('data\\median\\mix_data_simple_median_sort.xlsx')

# 保存成新数据
data = pd.DataFrame({'mix_data_0': mix_data_0_var,
                     'mix_data_50': mix_data_50_var,
                     'mix_data_100': mix_data_100_var},
                    # 'mix_data_simple': mix_data_simple_var},
                    index=mix_data_0.columns, columns=['mix_data_0', 'mix_data_50', 'mix_data_100'])


# ,'mix_data_simple'])


def rsr(data, weight=None, threshold=None, full_rank=True):
    Result = pd.DataFrame()
    n, m = data.shape

    # 对原始数据编秩
    if full_rank:
        for i, X in enumerate(data.columns):
            Result[f'X{str(i + 1)}：{X}'] = data.iloc[:, i]
            Result[f'R{str(i + 1)}：{X}'] = data.iloc[:, i].rank(method="dense")
    else:
        for i, X in enumerate(data.columns):
            Result[f'X{str(i + 1)}：{X}'] = data.iloc[:, i]
            Result[f'R{str(i + 1)}：{X}'] = 1 + (n - 1) * (data.iloc[:, i].max() - data.iloc[:, i]) \
                                           / (data.iloc[:, i].max() - data.iloc[:, i].min())

    # 计算秩和比
    weight = 1 / m if weight is None else np.array(weight) / sum(weight)
    Result['RSR'] = (Result.iloc[:, 1::2] * weight).sum(axis=1) / n
    Result['RSR_Rank'] = Result['RSR'].rank(ascending=False)

    # 绘制 RSR 分布表
    RSR = Result['RSR']
    RSR_RANK_DICT = dict(zip(RSR.values, RSR.rank().values))
    Distribution = pd.DataFrame(index=sorted(RSR.unique()))
    Distribution['f'] = RSR.value_counts().sort_index()
    Distribution['Σ f'] = Distribution['f'].cumsum()
    Distribution[r'\bar{R} f'] = [RSR_RANK_DICT[i] for i in Distribution.index]
    Distribution[r'\bar{R}/n*100%'] = Distribution[r'\bar{R} f'] / n
    Distribution.iat[-1, -1] = 1 - 1 / (4 * n)
    Distribution['Probit'] = 5 - norm.isf(Distribution.iloc[:, -1])

    # 计算回归方差并进行回归分析
    r0 = np.polyfit(Distribution['Probit'], Distribution.index, deg=1)
    print(sm.OLS(Distribution.index, sm.add_constant(Distribution['Probit'])).fit().summary())
    if r0[1] > 0:
        print(f"\n回归直线方程为：y = {r0[0]} Probit + {r0[1]}")
    else:
        print(f"\n回归直线方程为：y = {r0[0]} Probit - {abs(r0[1])}")

    # 代入回归方程并分档排序
    Result['Probit'] = Result['RSR'].apply(lambda item: Distribution.at[item, 'Probit'])
    Result['RSR Regression'] = np.polyval(r0, Result['Probit'])
    threshold = np.polyval(r0, [2, 4, 6, 8]) if threshold is None else np.polyval(r0, threshold)
    Result['Level'] = pd.cut(Result['RSR Regression'], threshold, labels=range(len(threshold) - 1, 0, -1))

    return Result, Distribution


def rsrAnalysis(data, file_name=None, **kwargs):
    Result, Distribution = rsr(data, **kwargs)
    file_name = 'data1\\RSR 分析结果报告.xlsx' if file_name is None else file_name + '.xlsx'
    Excel_Writer = pd.ExcelWriter(file_name)
    Result.to_excel(Excel_Writer, '综合评价结果')
    Result.sort_values(by='Level', ascending=False).to_excel(Excel_Writer, '分档排序结果')
    Distribution.to_excel(Excel_Writer, 'RSR分布表')
    Excel_Writer.save()

    return Result, Distribution


rsr(data)
rsrAnalysis(data)  # 保存结果

Result, Distribution = rsr(data)  # 取出结果

RSR_sort = Result['RSR'].sort_values(ascending=False)[1:31]  # 最终指标排名,选择前20个
RSR_sort.to_excel('data1\\前30.xlsx')  # 保存
