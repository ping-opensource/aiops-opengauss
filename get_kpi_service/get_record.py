# -*- coding: utf-8 -*-
import traceback
from sqlalchemy import Column
from sqlalchemy import DateTime, Float, Integer
from sqlalchemy.ext.declarative import declarative_base
from db_util import get_db_session, session_context
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)
"""
使用sqlalchemy创建数据库表模型的基本写法：
先给Base赋值，再使用类继承Base
"""
Base = declarative_base()


class SceneConfig(Base):
    """
    创建场景配置表的模型类
    """
    # __tablename__负责和数据库里的表关联起来
    __tablename__ = "failed_record"
    # id字段是整型，不能为空，能自增长，是主键
    id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    # thread_id字段是整型，不能为空，默认值为-1
    thread_id = Column(Integer, nullable=False, default=-1)  # -1是未知线程
    # timestamp字段是DateTime类型，不能为空
    timestamp = Column(DateTime, nullable=False)
    # value字段是浮点型，不能为空，默认值为0
    value = Column(Float, nullable=False, default=0)

    # 重写__repr__方法，使实例化对象输出为指定样式
    def __repr__(self):
        """format"""
        return "<ExternalOrderImportDetail(id={}, timestamp={}, thread_id={}, value={})>".format(self.id,
                                                                                                 self.timestamp,
                                                                                                 self.thread_id,
                                                                                                 self.value)

    def get_records(self):
        """
        查询所有记录行
        :return: 查询结果集合
        """
        scene_list = []
        # 将数据库名传递给get_db_session函数，使连接指定数据库，这里是连接了fault_locate数据库
        _session = get_db_session(db_name="fault_locate")
        with session_context(_session) as ss:
            """
            当 try 块没有出现异常时，程序会执行 else 块
            """
            try:
                # 查询指定数据库内SceneConfig关联的表中所有数据，这里关联表见第22行，即failed_record表
                result = ss.query(SceneConfig).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                for s in result:
                    scene = SceneConfig(id=s.id, timestamp=s.timestamp, thread_id=s.thread_id, value=s.value)
                    scene_list.append(scene)
        # 这里return的数据，格式如44行所设置
        return scene_list

    def get_record_with_scene_name(self, start_time=None):
        """ 条件查询 按场景名称查询 """
        ans = []
        _session = get_db_session(db_name="fault_locate")
        with session_context(_session) as ss:
            try:
                result = ss.query(SceneConfig).filter(SceneConfig.timestamp > start_time).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", result)
                # 构造对象 返回返回对象   返回指针是野指针
                if len(result) <= 0:  # 没有符合的场景
                    return ans
                for s in result:
                    scene = SceneConfig(id=s.id, timestamp=s.timestamp, thread_id=s.thread_id, value=s.value)
                    ans.append(scene)
                return ans
                # print ">>>> result:", result
        return ans

    def insert_record(self, new_scene=None):
        """
        插入记录
        req是请求的json文件 接口1使用 利用scene_id判断是否insert
        根据调用链判断  是否重复
        """
        if not new_scene:
            print("记录为空,插入失败")
            return

        _session = get_db_session(db_name="fault_locate")
        with session_context(_session) as ss:
            try:
                result = ss.add(new_scene)
            except:
                print("插入*****\n", traceback.format_exc())
            else:
                print(">>>>>result:", result)

    def get_record(self, value=None):
        """ 条件查询 按场景名称查询 """
        # 将数据库名传递给get_db_session函数，使连接指定数据库，这里是连接了fault_locate数据库
        _session = get_db_session(db_name="fault_locate")
        result = []
        with session_context(_session) as ss:
            try:
                """
                查询指定数据库内SceneConfig关联的表中的指定数据，这里关联表见第22行，即failed_record表
                查询出数据库中的value字段的值大于传进来的value的值的所有数据
                """
                result = ss.query(SceneConfig).filter(SceneConfig.value > value).all()
            except:
                print("查询*****\n", traceback.format_exc())
        return result


if __name__ == "__main__":
    obj = SceneConfig()
    s = SceneConfig(thread_id=123, value=1.2)
    print("\n\n{}\n\n\n".format(s))

    obj.insert_record(s)
    ans = obj.get_records()
    for s in ans:
        print(s)
