# -*- coding: utf-8 -*-
from flask import Flask

import threading
from flask import request
from const.config import UserNumber
from common.user_info import UserInfo

UserList = {}  # 记录所有用户  便于执行后续的命令
UserNum = 0  # 记录生成的用户数量
Mutex = threading.Lock()  # 全局锁

app = Flask(__name__)


def user_operator(**param):
    """
    模拟单个用户操作数据库
    """
    global UserList
    thread_id = param["thread_id"]
    user = UserInfo(id=thread_id)
    UserList[thread_id] = user
    user.run()


def run():
    try:
        print("UserNumber:{}".format(UserNumber))
        threadList = {}
        global UserNum
        for index in range(UserNumber):
            Mutex.acquire()
            UserNum += 1
            nowNum = UserNum
            Mutex.release()
            threadList[nowNum] = threading.Thread(target=user_operator, kwargs={"thread_id": nowNum})
            threadList[nowNum].start()
            print("线程{}开始操作".format(nowNum))
    except:
        print("Error: unable to start thread")


if __name__ == '__main__':
    t = threading.Thread(target=run)
    t.start()
    app.run(host="0.0.0.0", port=8168)
