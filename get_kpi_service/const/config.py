# -*- coding: utf-8 -*-

UserNumber = 1     # 模拟用户数量

OperatorNumber = 10000  # 用户操作次数

# 增删改查占比
OperatorRate = {
    "Insert": 0,     # 增加记录
    "Delete": 0,     # 删除记录
    "Update": 0,     # 更新记录
    "Select": 1,     # 查询记录
}

# 默认的增删改查占比 不要修改
DefaultOperatorRate = {
    "Insert": 0,     # 增加记录
    "Delete": 0,     # 删除记录
    "Update": 0,     # 更新记录
    "Select": 1,     # 查询记录
}

# 操作时间隔间范围 单位秒
OperatorGap = {
    "MaxGap": 1,        # 最长操作间隔
    "MinGap": 1,        # 最短操作间隔
}
