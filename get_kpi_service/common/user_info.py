# -*- coding: utf-8 -*-
from ..const.config import OperatorGap, DefaultOperatorRate
from ..const.config import OperatorNumber
from time import sleep
from ..get_record import SceneConfig
import random
import threading
import time

class UserInfo():
    """
    类构造器，在使用该类创建对象时，要传入一个id值，和一个字典
    """
    def __init__(self, id, **kwargs):
        """
        如果传入的字典有OperatorGap，就将值赋值给该变量，否则就把OperatorGap赋值给该变量，
        OperatorGap在service_get_zabbix_data.const.config下
        """
        self.OperatorGap = kwargs.get("OperatorGap") if kwargs.get("OperatorGap") else OperatorGap
        """
        如果传入的字典有DefaultOperatorRate，就将值赋值给该变量，否则就把DefaultOperatorRate赋值给该变量
        DefaultOperatorRate在service_get_zabbix_data.const.config下
        """
        self.OperatorRate = kwargs.get("DefaultOperatorRate") if kwargs.get("DefaultOperatorRate") else DefaultOperatorRate
        self.id = id
        self.database = SceneConfig()  # 连接数据库
        self.open = True                # 默认存活
        self.OperatorOpen = {           # 默认所有的操作都可执行
            "Insert": True,
            "Delete": True,
            "Update": True,
            "Select": True,
        }
        fileName = "data_{}.txt".format(int(self.OperatorRate["Insert"] * 100))
        self.OptFile = open(fileName, 'w')
    def run(self):
        # OperatorNumber为用户定义操作次数，在service_get_zabbix_data.const.config下
        for i in range(OperatorNumber):
            print (i)
            t = threading.Thread(target=self.db_opt_once)
            t.start()
            # 间隔一段时间继续下一次操作
            gapTime = self.get_time_gap()
            # print "线程{} 第{}次操作 间隔时间{}".format(self.id, i, gapTime).decode('UTF-8').encode('GBK')
            sleep(gapTime)
            # sleep(1)
        # print "线程{}结束操作".format(self.id).decode('UTF-8').encode('GBK')

    def db_opt_once(self):
        # 如果不存活就结束执行
        if not self.open:
            return
        now = int(time.time())
        # 调用next_operator函数，找出要执行的操作
        nextOpreator = self.next_operator()
        optNum = 0
        if nextOpreator == "Insert":
            value = random.random()
            self.database.insert_record(SceneConfig(thread_id=self.id, value=value))
            optNum = 1
        elif nextOpreator == "Delete":
            optNum = -1
            pass
        elif nextOpreator == "Update":
            optNum = -1
            pass
        elif nextOpreator == "Select":
            value = random.random()
            # 跳转到get_record函数
            self.database.get_record(value)
            optNum = 2
        print(self.OptFile)
        self.OptFile.write("{} {}\n".format(now, optNum))


    def next_operator(self):
        """
        返回值 None说明有错误
        否则返回字符串 代表下一个要进行的操作
        """
        # 随机生成一个0到1之间的浮点数
        rand_num = random.random()
        sumRate = 0
        # 找出要执行的操作
        for k, v in self.OperatorRate.items():
            sumRate += v
            if rand_num <= sumRate:
                return k
        return None

    def get_time_gap(self):
        """
        返回值 None说明有错误
        否则返回字符串 代表下一个要进行的操作
        """
        return 1.0 * random.randint(self.OperatorGap["MinGap"] * 1000, self.OperatorGap["MaxGap"] * 1000) / 1000.0
    def set_operator(self, newOperatorRate):
        # 判断每一个操作的占比是否正确 以及总和是否为1
        if not newOperatorRate:
            return "参数为空，设置失败", False
        sumRate = 0
        for k, v in newOperatorRate.items():
            if v < 0 or v > 1:
                print ("Error: {}操作占比为:{}".format(k, v))
                return "Error: {}操作占比为:{}".format(k, v), False
            sumRate += v
        if sumRate != 1:
            print ("Error: 操作占比总和不为1 实际为:{}".format(sumRate))
            return "Error: 操作占比总和不为1 实际为:{}".format(sumRate), False
        self.OperatorRate = newOperatorRate
        return "设置成功!", True

    def set_time_gap(self, newTimeGap):
        # 判断每一个操作的占比是否正确 以及总和是否为1
        if not newTimeGap:
            return "参数为空，设置失败", False
        for k, v in newTimeGap.items():
            if v < 0:
                return "参数不正确，设置失败", False
        self.OperatorGap = newTimeGap
        return "设置成功!", True