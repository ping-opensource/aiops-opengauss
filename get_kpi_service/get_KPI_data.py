# -*- coding: utf-8 -*-
import time
import traceback
from sqlalchemy import Column
from sqlalchemy import Float, Integer, BIGINT
from sqlalchemy.ext.declarative import declarative_base
from db_util import get_db_session, session_context
from const.config import OperatorRate
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

Base = declarative_base()

fp = None
invaildItemIdList = []


class ZabbixData(Base):
    """
    创建history的模型类
    """
    __tablename__ = "history"
    itemid = Column(BIGINT, nullable=False, default=-1, primary_key=True)
    clock = Column(Integer, nullable=False, default=-1, primary_key=True)
    value = Column(Float, nullable=False)
    ns = Column(Integer, nullable=False, default=0)

    # 重写__repr__方法，使实例化对象输出为指定样式
    def __repr__(self):
        """format"""
        return "<ExternalOrderImportDetail(itemid={}, timestamp={}, value={}, NS={})>".format(self.itemid,
                                                                                              self.clock,
                                                                                              self.value,
                                                                                              self.ns)

    # 调用此方法时，需要传递itemid，开始时间和结束时间
    def get_records(self, itemid, st, et):
        """
        查询所有记录行
        :param itemid: 对应zabbix数据库history表中的字段
        :param st: 开始时间
        :param et: 结束时间
        :return: 查询结果集合
        """
        scene_list = []
        # 连接zabbix的mysql数据库，以查询记录的数据
        _session = get_db_session(db_name="zabbix")  # 连接zabbix的mysql数据库，以查询记录的数据
        with session_context(_session) as ss:  # 调用session_context，方便session的操作
            try:
                # 筛选出特定时间内的值
                result = ss.query(ZabbixData).filter(ZabbixData.itemid == itemid) \
                    .filter(ZabbixData.clock >= st) \
                    .filter(ZabbixData.clock <= et).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                if len(result) == 0:
                    global invaildItemIdList
                    invaildItemIdList.append(itemid)
                for s in result:
                    global fp
                    # 将查询结果写入指定文件中
                    fp.write("{}\t{}\t{}\t{}\n".format(s.itemid, s.clock, s.value, s.ns))
                    scene = ZabbixData(itemid=s.itemid, clock=s.clock, value=s.value, ns=s.ns)
                    scene_list.append(scene)
        return scene_list


if __name__ == "__main__":
    obj = ZabbixData()
    itemid = ["10073", "10074", "10075", "10076", "10077",
              "10078", "23252", "23253", "23255", "23256",
              "23257", "23258", "23259", "23260", "23261",
              "23268", "23269", "23270", "23271", "23272",
              "23273", "23274", "23275", "23276", "23277",
              "23287", "23288", "23327", "23328", "23620",
              "23625", "23628", "23635", "23662", "23664",
              "25367", "25371", "25667", "25668", "28249",
              "28534", "28536", "28538", "29161", "29162",
              "23262", "23264", "23265", "23266", "23267",
              "29163", "29164", "29165", "29166", "29167",
              "29168", "29169", "29170", "29171", "29172",
              "29173", "29174", "29175", "29176", "29177",
              "29178", "29179", "29180", "29181", "29182",
              "29184", "29186", "29187", "29188", "29189",
              "29190", "29191", "29192", "29193", "29194",
              "29195", "29185", "29196", "29197", "29200",
              "29557", "29823", "31272", "34196", "36625",
              "36626", "36627", "36628", "36629", "36630",
              "36631", "36632", "36633", "36634", "36635",
              "36636", "36637", "36638", "36639", "36640",
              "36641", "36642", "36643", "36644", "36645"]
    start_time = "2023-02-04 16:00:00"
    end_time = "2023-02-06 14:00:00"
    for item in itemid:
        fileName = r".\zabbix_data\{}_DB_{}.txt".format(item, int(OperatorRate["Insert"] * 100))
        # 打开文件通道
        fp = open(fileName, 'w')
        # 将指定的时间变成用秒数来表示时间的浮点数（由于zabbix的mysql表中存储时间的形式就是浮点数）
        start_time_stamp = time.mktime(time.strptime(start_time, "%Y-%m-%d %H:%M:%S"))
        end_time_stamp = time.mktime(time.strptime(end_time, "%Y-%m-%d %H:%M:%S"))
        # start_time_stamp = 1668691584
        # end_time_stamp = 1668701584
        ans = obj.get_records(itemid=item, st=start_time_stamp, et=end_time_stamp)
        fp.close()
        print(r"invaildItemIdList: {}".format(invaildItemIdList))
    for s in ans:
        print(r's: {}'.format(s))
