# -*- coding: utf-8 -*-
# 服务3 预测服务 （P3）
#     1：每X分钟进行一轮预测
#     2：预测结果发送到服务1
import pandas as pd
from prophet import Prophet
from time import sleep
from get_KPI_data import ZabbixData
import time
import matplotlib.pyplot as plt
import requests

# 经过RSR排序后得到的前30个指标id
# TODO 这里是根据RSR算法排序生成的表格，手动添加的itemID
itemIDList = ["23275", "36636", "29174", "31272", "23620",
              "29177", "23253", "29175", "23256", "23257",
              "25667", "25668", "23276", "23274", "29170",
              "29171", "29166", "29162", "23264", "29173",
              "29200", "29823", "29172", "29176", "23273",
              "23277", "10076", "36626", "10077", "10073"]


def get_file_name(itemID=None):
    if not itemID:
        return None
    return ".\csv\{}.csv".format(itemID)

# 目前测试从csv获取数据 后续从数据库读取前x hour的数据
def load_csv_data(filename=""):
    """
    读取所有csv文件，没有csv文件就读取model文件
    """
    try:
        df = pd.read_csv(filename)  # 读取csv文件，获取csv文件中的所有内容
    except:
        df = pd.read_csv("model.csv")

    # df.head()  # 显示前面n行
    # for item in df.get('ds'):
    # print(df)
    return df


def save_csv_data(df, filename=""):
    df.to_csv(filename, index=0)


def fit_data(df="", itemID=""):
    m = Prophet() # 调取时序预测包
    m.fit(df)  # 拟合 模型训练
    future = m.make_future_dataframe(freq='H', periods=1)  # 预测天数  M月 H小时 D天
    future.tail() # 矩阵输出数据集

    forecast = m.predict(future) # 预测数据
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail() # 预测数据按矩阵输出
    # 直线预测
    fig1 = m.plot(forecast)
    # 趋势分解
    fig2 = m.plot_components(forecast)
    # 保存预测图像
    fig1.savefig(".\img\{}-1.png".format(itemId))
    fig2.savefig(".\img\{}-2.png".format(itemID))
    plt.close('all')
    return forecast


def get_last_timestamp(df):
    """
    得到最后一条记录的时间，更新后续记录。得不到则将时间赋值为当天的开始时间，即当天的零点
    """
    try:
        start_time = df.values[-1].tolist()[0]
        # 上面一行代码类似于下面两行：
        # last_line = df.values[-1]  得到csv文件的最后一行
        # start_time = last_line.tolist()[0]  将最后一行转换为列表类型，并获取列表的第一个数据（开始时间）
        start_time_stamp = time.mktime(time.strptime(start_time, "%Y/%m/%d %H:%M:%S"))  # 将从csv文件中获取的时间转换为时间戳
        return start_time_stamp
    except:
        # return "1648375203"  # 2022-01-23 12:00:001642910422
        return "1679562221"


def req_update_service1(itemID, minVal, maxVal):
    """
    service3利用prophet算法预测更新阈值以后，发送请求给service1，将阈值发送给service1
    """
    url = "http://{}:{}/set/?itemID={}&minVal={}&maxVal={}".format("127.0.0.1", 8101, itemID, minVal, maxVal)
    resp = requests.get(url)
    if resp.status_code != 200:
        print("请求失败")
    else:
        print("请求成功")


def update_item_warn_value(itemId, forecast):
    print("-------------------------------------------------------------")
    print("===============itemID: {}，forecast: {}===================".format(itemId, forecast))
    lastRow = forecast.values[-1]
    minVal = lastRow[2]
    maxVal = lastRow[3]
    print(minVal)
    print(maxVal)
    req_update_service1(itemId, minVal, maxVal)  # 调用


if __name__ == '__main__':
    mysqlClient = ZabbixData()  # 建立history表模型类的对象
    while True:
        for itemId in itemIDList:  # 遍历itemId列表
            fileName = get_file_name(itemId)  # 获取所有的csv文件名
            df = load_csv_data(filename=fileName)  # 读所有csv文件
            print("=======================读取的df内容及类型==============================")
            print(df, type(df))
            print("====================================================================")
            endTime = get_last_timestamp(df)  # 调用get_last_timestamp方法，获取时间戳
            print(endTime)

            recordList = mysqlClient.get_records(itemId, endTime)  # 更新数据库数据

            for record in recordList:  # 遍历查询zabbix数据库得到的数据
                print("============record:{}============".format(record))
                ds = time.strftime("%Y/%m/%d %H:%M:%S", time.localtime(record.clock))  # 将查询得到的数据的时间戳转换为指定的时间格式
                df = df.append({"ds": ds, "y": record.value}, ignore_index=True)  # 向读取的csv文件中添加字典数据，并且不使用index标签
            # 在调用save_csv_data方法时，如果文件内容小于两条，则会报错
            save_csv_data(df, fileName)   # 调用pandas包中的save_csv_data方法，将添加完成后的数据存储为csv文件
            # 对已有数据进行时序预测处理
            res = fit_data(df=df, itemID=itemId)
            update_item_warn_value(itemId=itemId, forecast=res)
        sleep(300)  # 每五分钟发送一次请求

# 1读取数据库数据 更新到文件
# 2预测新的阈值 1H
# 3发送HTTP请求更新阈值
