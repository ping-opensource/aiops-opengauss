# -*- coding: utf-8 -*-

import traceback
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from sqlalchemy.orm import sessionmaker, scoped_session

DB_CONN = None

DB_ENGINE = None
DB_SESSION = None

DB_RR_ENGINE = None
DB_RR_SESSION = None


def pick_engine(db_name="", mode_rr=False):
    DSN = "mysql+pymysql://collector:Collector_123@150.158.52.168:3306/{db_name}".format(
        db_name=db_name)
    DB_ENGINE = create_engine(DSN, poolclass=NullPool, encoding='utf-8', echo=True)
    if mode_rr:
        DB_RR_ENGINE = DB_ENGINE.execution_options(isolation_level="REPEATABLE READ")
        return DB_RR_ENGINE
    return DB_ENGINE


def get_db_session(db_name="", mode_rr=False):
    global DB_SESSION
    if DB_SESSION:
        return DB_SESSION
    session_factory = sessionmaker(bind=pick_engine(db_name))
    # scoped_session 线程安全
    DB_SESSION = scoped_session(session_factory)
    return DB_SESSION


def get_db_rr_mode_session(psm="", db_name="", mode_rr=False):
    """mode=repeatable read"""
    global DB_RR_SESSION
    if DB_RR_SESSION:
        return DB_RR_SESSION
    session_factory = sessionmaker(bind=pick_engine(db_name, True))
    DB_RR_SESSION = scoped_session(session_factory)
    return DB_RR_SESSION


@contextmanager
def session_context(session):
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise Exception(traceback.format_exc())
    finally:
        session.close()
