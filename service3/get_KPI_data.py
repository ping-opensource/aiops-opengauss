# -*- coding: utf-8 -*-
import time
import traceback
from sqlalchemy import Column
from sqlalchemy import Float, Integer, BIGINT
from sqlalchemy.ext.declarative import declarative_base
from db_util import get_db_session, session_context
import codecs

codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

Base = declarative_base()

class ZabbixData(Base):
    __tablename__ = "history"
    itemid = Column(BIGINT, nullable=False, default=-1,  primary_key=True)
    clock = Column(Integer, nullable=False, default=-1, primary_key=True)
    value = Column(Float, nullable=False)
    ns = Column(Integer, nullable=False, default=0)

    def __repr__(self):
        """format"""
        return "<ExternalOrderImportDetail(itemid={}, timestamp={}, value={}, NS={})>".format(self.itemid,
                                                                                          self.clock,
                                                                                          self.value,
                                                                                          self.ns)

    def get_records(self, itemid, st):
        """
        查询符合条件的记录行

        """
        scene_list = []
        _session = get_db_session(db_name="zabbix")
        with session_context(_session) as ss:
            try:
                result = ss.query(ZabbixData).filter(ZabbixData.itemid == itemid)\
                                             .filter(ZabbixData.clock > st).all()
            except:
                print("*****\n", traceback.format_exc())
            else:
                for s in result:
                    scene = ZabbixData(itemid=s.itemid, clock=s.clock, value=s.value, ns=s.ns)
                    scene_list.append(scene)
        return scene_list

if __name__ == "__main__":
    obj = ZabbixData()

