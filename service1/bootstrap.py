# -*- coding: utf-8 -*-

# 服务1 报警服务 （P1）
# 1：监听mysql数据库 获取新数据
# 2：和阈值比较，是否触发报警
# 3：监听端口，更新阈值

from flask import Flask
from flask import request
from get_KPI_data import ZabbixData
import threading
import requests
import json
from time import sleep, time

# 1.创建实例
app = Flask(__name__)

# zabbix监测项
#   指标id    指标解释
itemID2Name = {
    "23275": "Zabbix server: History index cache, % used",
    "36636": "/: Space utilization",
    "29174": "Load average (15m avg)",
    "31272": "Memory utilization",
    "23620": "Zabbix server: Value cache, % used",
    "29177": "Available memory in %",
    "23253": "Zabbix server: Utilization of configuration syncer internal processes, in %",
    "29175": "Load average (5m avg)",
    "23256": "Zabbix server: Utilization of escalator internal processes, in %",
    "23257": "Zabbix server: Utilization of history syncer internal processes, in %",
    "25667": "Zabbix server: Utilization of preprocessing manager internal processes, in %",
    "25668": "Zabbix server: Utilization of preprocessing worker internal processes, in %",
    "23276": "Zabbix server: Trend write cache, % used",
    "23274": "Zabbix server: History write cache, % used",
    "29170": "Load average (1m avg)",
    "29171": "CPU user time",
    "29166": "CPU softirq time",
    "29162": "CPU iowait time",
    "23264": "Zabbix server: Utilization of poller data collector processes, in %",
    "29173": "CPU idle time",
    "29200": "CPU utilization",
    "29823": "Zabbix server: Utilization of alert syncer internal processes, in %",
    "29172": "CPU system time",
    "29176": "Interrupts per second",
    "23273": "Zabbix server: Configuration cache, % used",
    "23277": "Zabbix server: Number of processed values per second",
    "10076": "Zabbix server: Number of processed numeric (unsigned) values per second",
    "36626": "vda: Disk write request avg waiting time (w_await)",
    "10077": "Zabbix server: Number of processed text values per second",
    "10073": "Zabbix server: Number of processed numeric (float) values per second"
}

# 经过RSR排序后得到的前30个指标id
itemIDList = [
    "23275", "36636", "29174", "31272", "23620",
    "29177", "23253", "29175", "23256", "23257",
    "25667", "25668", "23276", "23274", "29170",
    "29171", "29166", "29162", "23264", "29173",
    "29200", "29823", "29172", "29176", "23273",
    "23277", "10076", "36626", "10077", "10073"]

# 前30个KPI对应的指标ID和其默认值
# 此处的默认值统一设定，在接收service3的请求后，将自动更新
itemWarnValue = {
    "23275": [0, 100000],
    "36636": [0, 100000],
    "29174": [0, 100000],
    "31272": [0, 100000],
    "23620": [0, 100000],
    "29177": [0, 100000],
    "23253": [0, 100000],
    "29175": [0, 100000],
    "23256": [0, 100000],
    "23257": [0, 100000],
    "25667": [0, 100000],
    "25668": [0, 100000],
    "23276": [0, 100000],
    "23274": [0, 100000],
    "29170": [0, 100000],
    "29171": [0, 100000],
    "29166": [0, 100000],
    "29162": [0, 100000],
    "23264": [0, 100000],
    "29173": [0, 100000],
    "29200": [0, 100000],
    "29823": [0, 100000],
    "29172": [0, 100000],
    "29176": [0, 100000],
    "23273": [0, 100000],
    "23277": [0, 100000],
    "10076": [0, 100000],
    "36626": [0, 100000],
    "10077": [0, 100000],
    "10073": [0, 100000]
}


# 2.装饰器设置路由set，请求方式为get
# 接收service3发送的请求来更新阈值
@app.route('/set/', methods=["GET"])
def set():
    # get请求响应体
    res = {
        "code": 200,
        "message": "success",
        "data": "更新阈值失败"
    }
    # 从响应体中获取对应数据
    itemID = request.args.get("itemID", type=str, default="")
    minVal = request.args.get("minVal", type=float, default=-1)
    maxVal = request.args.get("maxVal", type=float, default=-1)
    if itemID == "" or minVal == -1 or maxVal == -1:
        return res
    # 创建全局变量itemIDList
    global itemIDList
    # 在itemIDList中查找itemID
    if itemID not in itemIDList:
        res["data"] = "itemID不存在"  # 将返回响应体中的data改为itemID不存在
        return res
    # 创建全局变量itemWarnValue
    global itemWarnValue
    itemWarnValue[itemID][0] = minVal  # 获取对应itemID的最小值
    itemWarnValue[itemID][1] = maxVal  # 获取对应itemID的最大值
    res["data"] = "设置新阈值成功"  # 将返回响应体中的data改为设置新阈值成功
    # format字符串格式化 对{}进行替换
    # 输出格式为：  itemID:[minVal,maxVal]
    print("{}:[{},{}]".format(itemID, minVal, maxVal))
    return res


def is_error(itemID=None, dataList=None):
    """
    判断数据库读取的最新值是否超过阈值
    错误返回-1
    正常返回0
    低于阈值返回1
    高于阈值返回2 同时对应下方遍历中的状态码
    """
    if not (itemID and dataList):  # 如果没有itemID并且没有dataList则返回错误值-1
        return -1, 0, 0, 0
    global itemWarnValue
    for data in dataList:
        # 低于阈值返回1
        if data.value < itemWarnValue[itemID][0]:
            return 1, data.value, itemWarnValue[itemID][0], itemWarnValue[itemID][1]
        # 高于阈值返回2
        elif data.value > itemWarnValue[itemID][1]:
            return 2, data.value, itemWarnValue[itemID][0], itemWarnValue[itemID][1]
    # 正常返回0
    return 0, 0, 0, 0


# 实例运行函数 在下方调用
def run():
    database = ZabbixData()  # 新建history表的模型类
    headers = {'Content-Type': 'application/json'}  # 响应头
    global itemIDList
    while True:
        et = int(time())  # 获取当前时间的时间戳作为结束的时间戳
        st = et - 60  # 开始时间为结束时间的60秒前
        errDataList = []  # 错误信息列表用来存储错误信息

        for itemID in itemIDList:
            # get_records自定义函数 查询所有记录行
            dataList = database.get_records(itemID, st, et)
            # 调用is_error方法，检查阈值是否正常
            errCode, val, minVal, maxVal = is_error(itemID, dataList)  # 调用is_error方法，返回4个值
            # 低于阈值为1 高于阈值为2
            if errCode in [1, 2]:  # 如果获得的errCode的值是1或者2
                # todo 调用关联模型 发送错误类型以及错误值阈值等信息
                # 错误信息返回的json字符串
                errData = {
                    "itemID": itemID,
                    "val": val,
                    "minVal": minVal,
                    "maxVal": maxVal,
                    "st": st,
                    "temp_jkd": (abs(val - minVal) + abs(val - maxVal) - (maxVal - minVal) / (2 * (maxVal - minVal)))
                    # 健康度定义公式
                }
                # open：打开文件
                # a+ :打开一个文件用于读写。如果该文件已存在，文件指针将会放在文件的结尾。文件打开时会是追加模式。如果该文件不存在，创建新文件用于读写。
                # 使用 with 关键字系统会自动调用 f.close() 方法 使用完毕会自动关闭文件 不会占用内存
                with open("errData.dat", "a+") as fp:
                    # fp文件中写入
                    fp.write("{} {} {} {} {} {}\n".format(errData["itemID"], errData["val"], errData["minVal"],
                                                          errData["maxVal"], errData["st"], errData["temp_jkd"]))
                # append 向数组中添加元素
                errDataList.append(errData)
        # 设置URL，向service4发送错误类型以及错误值阈值等信息
        URL = "http://127.0.0.1:8104{}".format("/aiops/get/error_list/")
        r = requests.post(url=URL, json=errDataList)
        # 每分钟运行一次
        sleep(60)


# 3.运行实例
if __name__ == '__main__':
    t = threading.Thread(target=run)
    t.start()
    app.run(host="0.0.0.0", port=8101)
